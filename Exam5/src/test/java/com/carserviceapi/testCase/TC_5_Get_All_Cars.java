package com.carserviceapi.testCase;

import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.carserviceapi.base.TestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_5_Get_All_Cars extends TestBase {
	
	@BeforeClass
	void getAllCars() throws InterruptedException {
		
		logger.info("======== TC_5_Get_All_Cars STARTED ========");
		
		RestAssured.baseURI = "https://5eaa20b5a873660016721073.mockapi.io/car";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/honda");
		
		Thread.sleep(3);
	}
	
	@Test(priority=0)
	void checkResponseBody() {
		
		logger.info("======== Checking Response Body ========");
		
		String responseBody = response.getBody().asString();
		logger.info("Response Body ==> " + responseBody);
		Assert.assertTrue(responseBody != null);
	}
	
	@Test(priority=1)
	void checkStatusCode() {
		
		logger.info("======== Checking Status Code ========");
		
		int statusCode = response.getStatusCode();
		logger.info("Status Code ==> " + statusCode);
		Assert.assertEquals(statusCode, 200);
	}
	
	@Test(priority=2)
	void checkResponseTime() {
		
		logger.info("======== Checking Response Time ========");
		
		long responseTime = response.getTime();
		logger.info("Response Time ==> " + responseTime + " ms");
		Assert.assertTrue(responseTime < 3000);
	}
	
	@Test(priority=3)
	void checkStatusLine() {
		
		logger.info("======== Checking Status Line ========");
		
		String statusLine = response.getStatusLine();
		logger.info("Status Line ==> " + statusLine);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
	}
	
	@Test(priority=4)
	void checkContentType() {
		
		logger.info("======== Checking Content Type ========");
		
		String contentType = response.header("Content-Type");
		logger.info("Content Type ==> " + contentType);
		Assert.assertEquals(contentType, "application/json");
	}
	
	@Test(priority=5)
	void checkServer() {
		
		logger.info("======== Checking Server Type ========");
		
		String server = response.header("Server");
		logger.info("Server Type ==> " + server);
		Assert.assertEquals(server, "Cowboy");
	}
	
	@Test(priority=6)
	void checkContentLength() {
		
		logger.info("======== Checking Content Length ========");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content Length ==> " + contentLength);
		
		if (Integer.parseInt(contentLength) < 100) {
			logger.warning("Content Length is less than 100");
		}
		
		Assert.assertTrue(Integer.parseInt(contentLength) > 100);
	}
	
	@AfterClass
	void tearDown() {
		
		logger.info("======== TC_5_Get_All_Cars FINISHED ========");
	}
}
