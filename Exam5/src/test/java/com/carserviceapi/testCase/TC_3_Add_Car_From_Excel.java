package com.carserviceapi.testCase;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.carserviceapi.base.TestBase;
import com.carserviceapi.utilities.ExcelUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_3_Add_Car_From_Excel extends TestBase {
	
	@BeforeClass
	void addCarExcel() throws InterruptedException {
		
		logger.info("======== TC_3_Add_Car_From_Excel STARTED ========");
		
		RestAssured.baseURI = "https://5eaa20b5a873660016721073.mockapi.io/car";
		httpRequest = RestAssured.given();
	}
	
	@DataProvider(name="data")
	Object[][] getDataEmpty() throws IOException {
		String path = "/Users/jovanandam/Documents/DANA/Eclipse/Exam5/excelData.xlsx";
		
		int rownum = ExcelUtils.getRowCount(path, "Sheet1");
		int colcount = ExcelUtils.getCellCount(path, "Sheet1", 1);
		
		Object dataEmployes[][] = new Object[rownum][colcount];
		
		for (int i = 1; i <= rownum; i++) {
			for (int j = 0; j < colcount; j++) {
				dataEmployes[i - 1][j] = ExcelUtils.getCellData(path, "Sheet1", i, j);
			}
		}
		
		return(dataEmployes);
	}
	
	@SuppressWarnings("unchecked")
	@Test(dataProvider="data")
	void checkResponse(String carType, String timeIn, String problem) {
		JSONObject requestParams = new JSONObject();
		requestParams.put("carType", carType);
		requestParams.put("timeIn", timeIn);
		requestParams.put("problem", problem);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.POST, "/honda");
		
		logger.info("======== Checking Response Body ========");
		
		String responseBody = response.getBody().asString();
		logger.info("Response Body ==> " + responseBody);
		Assert.assertTrue(responseBody != null);
		
		logger.info("======== Checking Status Code ========");
		
		int statusCode = response.getStatusCode();
		logger.info("Status Code ==> " + statusCode);
		Assert.assertEquals(statusCode, 201);
		
		logger.info("======== Checking Response Time ========");
		
		long responseTime = response.getTime();
		logger.info("Response Time ==> " + responseTime + " ms");
		Assert.assertTrue(responseTime < 3000);
		
		logger.info("======== Checking Status Line ========");
		
		String statusLine = response.getStatusLine();
		logger.info("Status Line ==> " + statusLine);
		Assert.assertEquals(statusLine, "HTTP/1.1 201 Created");
		
		logger.info("======== Checking Content Type ========");
		
		String contentType = response.header("Content-Type");
		logger.info("Content Type ==> " + contentType);
		Assert.assertEquals(contentType, "application/json");
		
		logger.info("======== Checking Server Type ========");
		
		String server = response.header("Server");
		logger.info("Server Type ==> " + server);
		Assert.assertEquals(server, "Cowboy");
		
		logger.info("======== Checking Content Length ========");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content Length ==> " + contentLength);
		
		if (Integer.parseInt(contentLength) < 50) {
			logger.warning("Content Length is less than 100");
		}
		
		Assert.assertTrue(Integer.parseInt(contentLength) > 50);
	}
	
	@AfterClass
	void tearDown() {
		
		logger.info("======== TC_3_Add_Car_From_Excel FINISHED ========");
	}
}
