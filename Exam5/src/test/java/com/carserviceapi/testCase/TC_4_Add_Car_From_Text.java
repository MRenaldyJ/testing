package com.carserviceapi.testCase;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.carserviceapi.base.TestBase;
import com.carserviceapi.utilities.Car;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_4_Add_Car_From_Text extends TestBase {
	
ArrayList<Car> carList = new ArrayList<>();
	
	@SuppressWarnings("unchecked")
	@BeforeClass
	void addCarText() throws InterruptedException {
		
		try {
			FileReader fr = new FileReader("/Users/jovanandam/Documents/DANA/Eclipse/Exam5/textData");
			BufferedReader br = new BufferedReader(fr);
			Scanner scFile = new Scanner(br);
			
			while (scFile.hasNextLine()) {
				String[] splitData = scFile.nextLine().split(",");
				Car car = new Car(splitData[0], splitData[1], splitData[2]);
				carList.add(car);
			}
			scFile.close();
			br.close();
			fr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("======== TC_4_Add_Car_From_Text STARTED ========");
		
		RestAssured.baseURI = "https://5eaa20b5a873660016721073.mockapi.io/car";
		httpRequest = RestAssured.given();
		
		JSONObject requestParams = new JSONObject();
		for (Car c : carList) {
			requestParams.put("carType", c.getCarType());
			requestParams.put("timeIn", c.getTimeIn());
			requestParams.put("problem", c.getProblem());
			
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(requestParams.toJSONString());
			
			response = httpRequest.request(Method.POST, "/honda");
		}
		Thread.sleep(3);
	}
	
	@Test(priority=0)
	void checkResponseBody() {
		
		logger.info("======== Checking Response Body ========");
		
		String responseBody = response.getBody().asString();
		logger.info("Response Body ==> " + responseBody);
		Assert.assertTrue(responseBody != null);
	}
	
	@Test(priority=1)
	void checkStatusCode() {
		
		logger.info("======== Checking Status Code ========");
		
		int statusCode = response.getStatusCode();
		logger.info("Status Code ==> " + statusCode);
		Assert.assertEquals(statusCode, 201);
	}
	
	@Test(priority=2)
	void checkResponseTime() {
		
		logger.info("======== Checking Response Time ========");
		
		long responseTime = response.getTime();
		logger.info("Response Time ==> " + responseTime + " ms");
		Assert.assertTrue(responseTime < 3000);
	}
	
	@Test(priority=3)
	void checkStatusLine() {
		
		logger.info("======== Checking Status Line ========");
		
		String statusLine = response.getStatusLine();
		logger.info("Status Line ==> " + statusLine);
		Assert.assertEquals(statusLine, "HTTP/1.1 201 Created");
	}
	
	@Test(priority=4)
	void checkContentType() {
		
		logger.info("======== Checking Content Type ========");
		
		String contentType = response.header("Content-Type");
		logger.info("Content Type ==> " + contentType);
		Assert.assertEquals(contentType, "application/json");
	}
	
	@Test(priority=5)
	void checkServer() {
		
		logger.info("======== Checking Server Type ========");
		
		String server = response.header("Server");
		logger.info("Server Type ==> " + server);
		Assert.assertEquals(server, "Cowboy");
	}
	
	@Test(priority=6)
	void checkContentLength() {
		
		logger.info("======== Checking Content Length ========");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content Length ==> " + contentLength);
		
		if (Integer.parseInt(contentLength) < 50) {
			logger.warning("Content Length is less than 100");
		}
		
		Assert.assertTrue(Integer.parseInt(contentLength) > 50);
	}
	
	@AfterClass
	void tearDown() {
		
		logger.info("======== TC_4_Add_Car_From_Text FINISHED ========");
	}
}
