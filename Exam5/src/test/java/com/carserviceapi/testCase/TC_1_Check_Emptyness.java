package com.carserviceapi.testCase;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.carserviceapi.base.TestBase;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_1_Check_Emptyness extends TestBase {

	@BeforeClass
	void checkEmptyness() throws InterruptedException {
		
		logger.info("======== TC_1_Check_Emptyness STARTED ========");
		
		RestAssured.baseURI = "https://5eaa20b5a873660016721073.mockapi.io/car";
		httpRequest = RestAssured.given();
		
		response = httpRequest.request(Method.GET, "/honda");
	}
	
	@Test
	void checkContentLength() {
		
		logger.info("======== Checking Content Length ========");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content Length ==> " + contentLength);
		
		if (Integer.parseInt(contentLength) > 10) {
			logger.warning("Content Length is bigger than 10 ==> API is not Empty");
		}
		
		Assert.assertTrue(Integer.parseInt(contentLength) < 10);
	}
	
	@AfterClass
	void tearDown() {
		
		logger.info("======== TC_1_Check_Emptyness FINISHED ========");
	}
}
