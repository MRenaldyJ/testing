package com.carserviceapi.utilities;

public class Car {
	
	private String carType;
	private String timeIn;
	private String problem;
	
	public Car(String carType, String timeIn, String problem) {
		super();
		this.carType = carType;
		this.timeIn = timeIn;
		this.problem = problem;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

}
